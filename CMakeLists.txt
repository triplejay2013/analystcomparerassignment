cmake_minimum_required(VERSION 3.6)
project(AnalystComaprer)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES src/main.cpp src/utility/Utils.cpp src/utility/Utils.h
        src/Comparer.cpp src/Comparer.h src/Analyst.cpp src/Analyst.h src/PurchaseSaleTransaction.cpp
        src/PurchaseSaleTransaction.h src/History.cpp src/History.h
        src/utility/ColumnDefinition.cpp src/utility/ColumnDefinition.h
        src/utility/FormattedCell.cpp src/utility/FormattedCell.h
        src/utility/FormattedTable.h src/utility/FormattedTable.cpp
        src/utility/FormattedRow.cpp src/utility/FormattedRow.h)
add_executable(AnalystComaprer ${SOURCE_FILES})