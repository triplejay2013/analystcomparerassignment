//===================================================================
// Name:        PurchaseSaleTransaction.h
// Author:      Justin Johnson
// Version:     1.0
// Description: Header File
//===================================================================

#ifndef ANALYSTCOMAPRER_PURCHASESALETRANSACTION_H
#define ANALYSTCOMAPRER_PURCHASESALETRANSACTION_H
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>

using namespace std;

class PurchaseSaleTransaction {
public:
    //Constructor
    PurchaseSaleTransaction(ifstream& ifs);
    string getStockSymbol();
    int getPurchaseDate();
    int getSaleDate();
    float computeProfitLoss();
    float computeInvestmentAmount();
private:
    string m_stockSymbol;
    int m_quantity;
    int m_purchaseDate;
    int m_purchasePrice;
    int m_purchaseTransFee;
    int m_saleDate;
    int m_salePrice;
    int m_saleTransFee;
};
#endif //ANALYSTCOMAPRER_PURCHASESALETRANSACTION_H
