//===================================================================
// Name:        main.cpp
// Author:      Justin Johnson
// Version:     1.0
// Description: AnalystComparer
//===================================================================
#include <iostream>
#include "Comparer.h"

using namespace std;

int main(int argc, char* argv[])
{
    Comparer compare;
    if(compare.load(argc, argv) == 0){
        compare.compare();
    }

    return 0;
}
