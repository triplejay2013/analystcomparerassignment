//===================================================================
// Name:        PurchaseSaleTransaction.cpp
// Author:      Justin Johnson
// Version:     1.0
// Description: A complete record of purchases/sales with given stock symbol
//===================================================================

#include "PurchaseSaleTransaction.h"

//Constructor
//@param input filestream
PurchaseSaleTransaction::PurchaseSaleTransaction(ifstream& ifs) {
    string tmp;
    getline(ifs, tmp);
    stringstream ss(tmp);
    int cnt = 1;
    int tmpInt;
    while (getline(ss, tmp, ',')) {
        switch (cnt) {
            //symbol
            case 1:
                m_stockSymbol = tmp;
                break;
            //quantity
            case 2:
                tmpInt = stoi(tmp);
                m_quantity = tmpInt;
                break;
            //purchase date
            case 3:
                tmpInt = stoi(tmp);
                m_purchaseDate = tmpInt;
                break;
            //purchase price
            case 4:
                tmpInt = stoi(tmp);
                m_purchasePrice = tmpInt;
                break;
            //purchase trans fee
            case 5:
                tmpInt = stoi(tmp);
                m_purchaseTransFee = tmpInt;
                break;
            //sale date
            case 6:
                tmpInt = stoi(tmp);
                m_saleDate = tmpInt;
            break;
            //sale price
            case 7:
                tmpInt = stoi(tmp);
                m_salePrice = tmpInt;
                break;
            //sale trans fee
            case 8:
                tmpInt = stoi(tmp);
                m_saleTransFee = tmpInt;
                cnt = 0;
                break;

            default:
                break;
        }
        cnt++;
    }
}

//getter
int PurchaseSaleTransaction::getPurchaseDate() {return m_purchaseDate;}

//getter
int PurchaseSaleTransaction::getSaleDate() {return m_saleDate;}

//getter
string PurchaseSaleTransaction::getStockSymbol() {return m_stockSymbol;}

//calculates profit loss for that stock
float PurchaseSaleTransaction::computeProfitLoss() {
    if(!this){return 0;}
    float profitLoss = (m_quantity * m_salePrice) - computeInvestmentAmount();

    return profitLoss;
}

//calculates how much Analyst invested into a stock
float PurchaseSaleTransaction::computeInvestmentAmount() {
    if(!this){return 0;}
    int investment = (m_quantity * m_purchasePrice) + m_purchaseTransFee + m_saleTransFee;

    return (float) investment;
}
