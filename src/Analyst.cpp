//===================================================================
// Name:        Analyst.cpp
// Author:      Justin Johnson
// Version:     1.0
// Description: Analyst for each input file
//===================================================================

#include "Analyst.h"

//Default Constructor
Analyst::Analyst() {m_history = new History();}

//getter
string Analyst::getName() {return m_name;}

//getter
string Analyst::getInitials() {return m_initials;}

//getter
History* Analyst::getHistory() {return m_history;}

//Parses file stream into member variables
//Creates a History
//return -1 for failed input return 0 for success
int Analyst::load(ifstream& ifs) {
    if(!ifs){return -1;}
    string tmpStr;

    //>>analysts full name //1st line of file
    getline(ifs, tmpStr);
    m_name = tmpStr;

    //>>analysts initials //2nd line of file
    getline(ifs, tmpStr);
    m_initials = tmpStr;

    if(m_history->load(ifs) < 0){return -1;}

    return 0;
}

//calculates performance of any given stock
float Analyst::getStockPerformance(string symbol) {
    float stockPerformance = 0;
    for(PurchaseSaleTransaction* pst: getHistory()->getPST()){
        if(pst->getStockSymbol() == symbol){
            stockPerformance = getHistory()->computeTotalProfitLossPerDay(symbol);
            return stockPerformance;
        }
    }
    return 0;
}
