//===================================================================
// Name:        History.cpp
// Author:      Justin Johnson
// Version:     1.0
// Description: Holds a history of all purchases/sales
//===================================================================

#include <sstream>
#include "History.h"

//getter
int History::getDays() {return m_days;}

//getter
int History::getSeedMoney() {return m_seedMoney;}

//getter
vector<PurchaseSaleTransaction*> History::getPST() {return m_pst;}

//parses data into member variables and creates purchase sale transactions
//return -1 if failed to load
//return 0 for success
int History::load(ifstream& ifs) {
    if(!ifs){return -1;}
    string tmpStr;
    int tmpInt = 0;

    //>>number of days //3rd line of file
    getline(ifs, tmpStr);
    try{
        tmpInt = stoi(tmpStr);
    }catch(...){return -1;}
    m_days = tmpInt;

    //>>initial seed money //4th line of file
    getline(ifs, tmpStr);
    try{
        tmpInt = stoi(tmpStr);
    }catch(...){return -1;}
    m_seedMoney = tmpInt;

    //>>number of purchase sales //5th line of file
    getline(ifs, tmpStr);
    try{
        tmpInt = stoi(tmpStr);
    }catch(...){return -1;}
    m_numPS = tmpInt;

    //m_numPS read from file
    //create m_numPS number of PurchaseSaleTransactions*
    for(int i = 0; i < m_numPS; ++i){
        PurchaseSaleTransaction *pst = new PurchaseSaleTransaction(ifs);
        m_pst.push_back(pst);
    }

    return 0;
}

//calculates profit loss per day for given analyst
float History::computeProfitLossPerDay() {
    if(!this){return 0;}
    float totalLoss = computeTotalProfitLoss();
    float lossPerDay = totalLoss/m_days;

    return (lossPerDay);
}

//Calculates total profit loss for an Analyst
float History::computeTotalProfitLoss() {
    if(!this){return 0;}
    float totalProfitLoss = 0;
    float tmp;
    for(PurchaseSaleTransaction* pst: m_pst){
        tmp = pst->computeProfitLoss();
        totalProfitLoss += tmp;
    }

    return totalProfitLoss/100;
}

//calculates total profit lost per day for a given symbol
float History::computeTotalProfitLossPerDay(string symbol) {
    if(!this){return 0;}
    float totalProfitLoss = 0;
    float tmp = 0;
    for(PurchaseSaleTransaction* pst: m_pst){
        if(pst->getStockSymbol() == symbol){
            tmp = pst->computeProfitLoss();
            totalProfitLoss += tmp;
        }
    }
    totalProfitLoss /= getTotalDays(symbol);
    totalProfitLoss /= 100;

    return totalProfitLoss;
}

//calculates total days between a purchase and sale time
float History::getTotalDays(string symbol) {
    float totalDays = 0;
    float maxTime = 0;
    float minTime = INT32_MAX;
    if(!this){return 0;}
    for(PurchaseSaleTransaction* pst: m_pst){
        if(pst->getStockSymbol() == symbol) {
            if(pst->getSaleDate() > maxTime) {maxTime = pst->getSaleDate();}
            if(pst->getPurchaseDate() < minTime) {minTime = pst->getPurchaseDate();}
        }
    }
    totalDays = ((maxTime - minTime)/(1440));//24*60

    return totalDays;
}
