//===================================================================
// Name:        Analyst.h
// Author:      Justin Johnson
// Version:     1.0
// Description: Header File
//===================================================================

#ifndef ANALYSTCOMAPRER_ANALYST_H
#define ANALYSTCOMAPRER_ANALYST_H
#include <iostream>
#include <string>
#include <fstream>
#include "History.h"

using namespace std;

class Analyst {
public:
    //Default constructor
    Analyst();
    //setters and getters
    string getName();
    string getInitials();
    History* getHistory();
    int load(ifstream& ifs);
    float getStockPerformance(string symbol);
private:
    string m_name;//analysts full name
    string m_initials;//analysts initials
    History* m_history;//a history of Purchase Sale Transactions for this Analyst
};
#endif //ANALYSTCOMAPRER_ANALYST_H
