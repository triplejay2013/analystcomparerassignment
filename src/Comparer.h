//===================================================================
// Name:        Comparer.h
// Author:      Justin Johnson
// Version:     1.0
// Description: Header File
//===================================================================

#ifndef ANALYSTCOMAPRER_COMPARER_H
#define ANALYSTCOMAPRER_COMPARER_H

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cstring>
#include <sstream>
#include <algorithm>
#include "Analyst.h"
#include "utility/FormattedTable.h"
#include "PurchaseSaleTransaction.h"

using namespace std;

class Comparer {
private:
    ifstream            m_ifs;//input file
    vector<Analyst*>    m_analysts;
    int                 m_analystCount;//number of analysts read in
    int                 m_symbolsCount;//number of unique symbols
    string              m_outputFileName;
    string              m_symbols[100];//array of unique symbols
public:
    int load(int argc, char* argv[]);
    int compare() const;
private:
    void loadSymbols();
    void outputInvestorsNames(ofstream& ofs) const;
    void outputOverallPerformance(ofstream& ofs) const;
    void outputStockPerformance(ofstream& ofs) const;
};
#endif //ANALYSTCOMAPRER_COMPARER_H
