//===================================================================
// Name:        History.h
// Author:      Justin Johnson
// Version:     1.0
// Description: Header File
//===================================================================

#ifndef ANALYSTCOMAPRER_HISTORY_H
#define ANALYSTCOMAPRER_HISTORY_H


#include <iostream>
#include <vector>
#include <fstream>
#include "PurchaseSaleTransaction.h"

class History {
public:
    int load(ifstream& ifs);
    int getDays();
    int getSeedMoney();
    float getTotalDays(string symbol);
    float computeProfitLossPerDay();
    float computeTotalProfitLoss();
    float computeTotalProfitLossPerDay(string symbol);
    vector<PurchaseSaleTransaction*> getPST();
private:
    int m_days;//number of days since 1/1/2016
    int m_seedMoney;//initial money
    int m_numPS;//number of purchase sales
    vector<PurchaseSaleTransaction*> m_pst;
};
#endif //ANALYSTCOMAPRER_HISTORY_H
