//===================================================================
// Name:        Comparer.cpp
// Author:      Justin Johnson
// Version:     1.0
// Description: Compares a set of Analysts
//===================================================================

#include "Comparer.h"

int Comparer::load(int argc, char* argv[]) {
    if(argc <= 2){
        cerr << "Not enough program arguements\n";
        cerr << "You inputed " << argc - 1 << " Arguement(s)\n";
        cerr << "Program needs at least 2 Arguements\n";
        return -1;
    }
    //saves output file
    m_outputFileName = argv[1];
    //saves number of analysts arg[i>=2]
    m_analystCount = argc-2;
    for (int i=2; i<=argc; ++i){
        //check if valid input file stream
        if(argv[i]!=NULL){
            m_ifs.open(argv[i]);
            if(m_ifs){
                //create analyst
                Analyst* a = new Analyst();
                //load data
                if(a->load(m_ifs) < 0){
                    cerr << "Failed to Load \"" << argv[i] << "\"" <<endl;
                    return -1;
                }
                //add to vector<Analyst*>
                m_analysts.push_back(a);
            }
            else{
                cerr<<"FILE COULD NOT BE OPENED\n";
                cerr<<"Error Code: " << strerror(errno) << endl;
                return -1;
//                exit(1);
            }
            //must close ifstream before opening a new one
            m_ifs.close();
        }
    }
    loadSymbols();
    return 0;
}

//Loads Unique Symbols into an array
void Comparer::loadSymbols() {
    m_symbolsCount = 0;
    for(int i = 0; i < m_analystCount; ++i){
        //get Analyst's History
        History* history = m_analysts[i]->getHistory();
        string symbol;
        //iterate through history's PST to find unique symbols
        for(PurchaseSaleTransaction* pst:history->getPST()){
            symbol = pst->getStockSymbol();
            //Fill Symbols->m_symbols[] with unique symbol
            //for each repeated symbol, add total profit loss, and total days
            string *existingSymbol = find(begin(m_symbols), end(m_symbols), symbol);
            //if a symbol is unique
            if (existingSymbol == end(m_symbols)) {
                //fills m_symbols with unique symbols
                m_symbols[m_symbolsCount++] = symbol;
            }
        }
    }
}

//Checks if comparison can be done and then writes to file
int Comparer::compare() const {
    if(m_analystCount < 2){
        cout << "Cannot do comparison with " << m_analystCount << " analysts" << endl;
        return -1;
    }

    if(m_outputFileName == ""){
        cout << "Cannot do comparison because no output file was specified" << endl;
        return -1;
    }

    ofstream ofs(m_outputFileName);
    outputInvestorsNames(ofs);
    outputOverallPerformance(ofs);
    outputStockPerformance(ofs);
    ofs.close();

    return 0;
}

//Header for output with List of Analysts
void Comparer::outputInvestorsNames(ofstream &ofs) const {
    ofs << "Analyst Comparison" << endl;
    ofs << "==================\n\n";
    ofs << "Investors:\n";
    for(Analyst* a: m_analysts){
        ofs << "\t" << a->getName().c_str() << endl;
    }
}

//Prints out overall performance table
void Comparer::outputOverallPerformance(ofstream &ofs) const {
    ofs << "\nOverall Performance:\n";
    FormattedTable table(4, m_analystCount+1);
    //declare number of rows and columns

    table.addColumn(new ColumnDefinition("", 15, ColumnDefinition::String, ColumnDefinition::Left));
    for(Analyst* a: m_analysts){
        table.addColumn(new ColumnDefinition(a->getInitials(), 12, ColumnDefinition::FixedPrecision, ColumnDefinition::Right, 2));
    }

    FormattedRow* row = new FormattedRow(&table);
    row->addCell(new FormattedCell("D_(days)"));
    for(Analyst* a: m_analysts){
        row->addCell(new FormattedCell((float) a->getHistory()->getDays()));
    }
    table.addRow(row);

    row = new FormattedRow(&table);
    row->addCell(new FormattedCell("Seed Amount ($)"));
    for(Analyst* a: m_analysts){
        row->addCell(new FormattedCell((float) a->getHistory()->getSeedMoney()/100));
    }
    table.addRow(row);

    row = new FormattedRow(&table);
    row->addCell(new FormattedCell("TPL ($)"));
    for(Analyst* a: m_analysts){
        //TPL - total profit loss
        row->addCell(new FormattedCell(a->getHistory()->computeTotalProfitLoss()));
    }
    table.addRow(row);

    row = new FormattedRow(&table);
    row->addCell(new FormattedCell("PLPD ($)"));
    for(Analyst* a: m_analysts){
        row->addCell(new FormattedCell(a->getHistory()->computeProfitLossPerDay()));
    }
    table.addRow(row);
    table.write(ofs);
}

//outputs stock performance table
void Comparer::outputStockPerformance(ofstream &ofs) const {
    ofs << "\nStock Performance:\n";
    ofs << "\t\t\t\t\t\tSPLPD's\n\n";
    FormattedTable table(m_symbolsCount, m_analystCount+1);
    table.addColumn(new ColumnDefinition("", 7, ColumnDefinition::String, ColumnDefinition::Left));

    for(Analyst* a: m_analysts){
        table.addColumn(new ColumnDefinition(a->getInitials(), 12, ColumnDefinition::FixedPrecision, ColumnDefinition::Right, 4));
    }

    FormattedRow* row;
    int cnt = 0;
    for(string symbol: m_symbols){
        row = new FormattedRow(&table);
        if(cnt < m_symbolsCount){
            row->addCell(new FormattedCell(symbol));
            for(Analyst* a: m_analysts){
                row->addCell(new FormattedCell(a->getStockPerformance(symbol)));
            }
            table.addRow(row);
        }
        cnt++;
    }
    table.write(ofs);
}
